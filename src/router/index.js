/**
 * 路由文件，路由映射
 */

import Vue from 'vue';
import VueRouter from 'vue-router';
import contacts from './modules/contacts';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  // 登录页面
  {
    path: '/login',
    component: () => import('@/views/login')
  },
  // 首页
  {
    path: '/home',
    component: () => import('@/views/home'),
  },
  // 应用
  {
    path: '/application/:appId',
    component: () => import('@/layout/application'),
    children:[
      // {
      //   path: '/',
      //   redirect: '/application/:appId/common'
      // },
      {
        path: 'common',
        component: () => import('@/views/common'),
      },
      {
        path: 'sys-role',
        component: () => import('@/views/sys-role'),
      },
      {
        path: 'sys-menu',
        component: () => import('@/views/sys-menu'),
      },
      {
        path: 'manage',
        component: () => import('@/views/create-app/manage'),
      },
      {
        path: 'common/:busiKey',
        component: () => import('@/layout/application/dashboard')
      }
    ]
  },

  // 新建-编辑应用
  {
    path: '/nuwaform',
    component: () => import('@/layout/nuwa-form'),
    children: [
      {
        path: '/',
        name: 'nuwaform',
        redirect: '/nuwaform/design'
      },
      {
        path: 'design',
        name: '/nuwaform/design',
        component: () => import('@/views/design/Home.vue')
      },
      {
        path: 'design/:appId',
        name: '/nuwaform/design',
        component: () => import('@/views/design/Home.vue')
      },
      {
        path: 'design/:appId/:busiKey',
        name: '/nuwaform/design',
        component: () => import('@/views/design/Home.vue')
      },
      {
        path: 'process',
        name: '/nuwaform/process',
        component: () => import('@/views/process')
      },
      {
        path: 'extension',
        name: '/nuwaform/extension',
        component: () => import('@/views/extension')
      },
      {
        path: 'publish',
        name: '/nuwaform/publish',
        component: () => import('@/views/publish')
      },
      {
        path: 'dataManage',
        name: '/nuwaform/dataManage',
        component: () => import('@/views/dataManage')
      }
    ]
  },
  
  // 通讯录
  contacts,
  
  // 404页面
  { 
    path: '/:pathMatch(.*)*', 
    name: 'NotFound', 
    component: () => import('@/views/error-page/404.vue') 
  }
]

 const router = new VueRouter({
  base: window.__POWERED_BY_QIANKUN__ ? '/app-vue/' : '/',
  mode: 'hash',
  routes
})

export default router
