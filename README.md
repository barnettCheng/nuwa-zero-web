# nuwa-zero-web

## 介绍

> 基于Vue CLI搭建，node version 10.13.0 +

## 使用教程

### 安装依赖

```
npm install / npm i
```

### 启动本地开发环境

```
npm run dev / npm start
```
### 启动mock服务

```
npm run mock
```


### 构建生产环境

```
npm run build
```



