/**
 * @description:应用模块下状态管理数据
 * @author:zhangmanyi
 */

 import { getAllApp } from '@/api/home'

const state = {
  isFold: false,  //导航是否折叠
  appId:'', //应用id
  apps:[],//轻应用
  innerApps:[] //内置应用
}

const mutations = {
  SET_ISFOLD (state,payload) {
    state.isFold = payload
  },
  SET_APPLICATIONID (state,payload){
    state.appId = payload
  },
  SET_APPS: (state, payload) => {
    state.apps = payload
  },
  SET_INNERAPPS: (state, payload) => {
    state.innerApps = payload
  }
}

const actions = {
  // 获取首页应用
  getAllApp({commit}) {
    
    getAllApp().then(res => {
      commit('SET_APPS',res.data['1'])
      commit('SET_INNERAPPS',res.data['2'])
    }).catch(error => {
      console.log(error)
    })
   
  }
}
const getters = {
  allApps: state => [...state.apps,...state.innerApps],
  // 跳转的应用是否为内置应用
  isInnerApp:state => {return state.innerApps.map(item=>item.id).includes(state.appId)}
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
