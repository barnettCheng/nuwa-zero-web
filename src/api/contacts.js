/**
 * @description:通讯录下服务函数
 * @author:zhangmanyi
 */

import request from '@/utils/request'

 // 获取用户菜单信息列表
export function getRoleGroupAndRoleInfo (data) {
  return request({
    url: '/base/nuwa-sys-role/queryRoleGroupAndRoleInfoList',
    method: 'post',
    data
  })
}

 // 角色组--保存功能
export function saveRoleGroup (data) {
  return request({
    url: '/base/nuwa-sys-role/saveRoleGroup',
    method: 'post',
    data
  })
}

// 角色组--保存功能
export function deleteRole (data) {
  return request({
    url: '/nuwa-sys-role/deleteRole',
    method: 'post',
    data
  })
}
// 角色组--修改功能
export function updateRoleGroup (data) {
  return request({
    url: '/nuwa-sys-role/updateRoleGroup',
    method: 'post',
    data
  })
}


 