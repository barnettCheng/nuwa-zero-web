const state = {
  navName: '未命名表单',
  formHistory: {},
  subformActiveId: {},
  subFormInfo: [],
  subFormFlag: ''
}

const mutations = {
  SET_NAVNAME (state, payload) {
    state.navName = payload
  },
  SET_SUBFORMACTIVEID (state, payload) {
    state.subformActiveId = payload
  },
  SET_SUBFORMINFO (state, payload) {
    if(state.subFormInfo.length == 0) {
      state.subFormInfo.push({formId: payload.formId, value: payload.value})
    }else {
      let index = state.subFormInfo.findIndex(item => item.formId == payload.formId)
      if(index >= 0) {
        state.subFormInfo[index] = {formId: payload.formId, value: payload.value}
      }else {
        state.subFormInfo.push({formId: payload.formId, value: payload.value})
      }
    }
    console.log(state.subFormInfo)
  },
  SET_SUBFORMFLAG(state, payload) {
    state.subFormFlag = payload
  }
}


export default {
  namespaced: true,
  state,
  mutations
}
