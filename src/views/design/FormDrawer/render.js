const model = require.context('./components', true, /\.vue$/)
let components = {}
model.keys().forEach(item => {
  let comp = model(item)
  console.log(item)
  components[comp.default.name] = comp.default
})
export default {
  render (h, cxt) {
    return h(require(components), {
      props: {
        prop: this.prop
      }
    })
  }
}