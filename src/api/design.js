import request from '@/utils/request'

export function selectMetaDataList (params) {
  return request({
    url: '/base/api/meta-data-type/selectMetaDataList',
    method: 'get',
    params
  })
}

export function selectForm (params) {
  return request({
    url: '/base/api/form-field-attr/detail',
    method: 'get',
    params
  })
}

export function login (params) {
  return request({
    url: '/base/api/form/getFieldAttr',
    method: 'get',
    params
  })
}

export function addApplication (data) {
  return request({
    url: '/base/api/application-manager/edit',
    method: 'post',
    data
  })
}

export function saveOrUpdate (data) {
  return request({
    url: '/base/api/form-field-attr/saveOrUpdate',
    method: 'post',
    data
  })
}
