/**
 * @description:角色管理服务函数
 * @author:zhangmanyi
 */

 import request from '@/utils/request'


 // 保存角色信息
export function saveRole(data) {
  return request({
    url: '/nuwa-sys-role/saveRole',
    method: 'post',
    data
  })
}

 // 删除角色信息
export function batchDeleteRole(data) {
  return request({
    url: '/base/nuwa-sys-role/batchDeleteRole',
    method: 'post',
    data
  })
}

 // 修改角色信息
export function updateRole(data) {
  return request({
    url: '/base/nuwa-sys-role/updateRole',
    method: 'post',
    data
  })
}

 // 分页查询角色信息
export function queryRolePage(data) {
  return request({
    url: '/base/nuwa-sys-role/queryRolePage',
    method: 'post',
    data
  })
}
export function queryAllList(data) {
  return request({
    url: '/nuwa-sys-role/queryAllList',
    method: 'post',
    data
  })
}
