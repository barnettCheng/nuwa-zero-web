import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

const messages = {
  zh_CN: require('./lang/zh'),
  en_US: require('./lang/en'),
}

const i18n = new VueI18n({
  locale: 'zh_CN',    // 语言标识
  silentFallbackWarn:true,
  silentTranslationWarn: true,
  messages
})

export default i18n