/**
 * @description:应用模块下服务函数
 * @author:zhangmanyi
 */

import request from '@/utils/request'

// 获取用户菜单信息列表
export function getMenuList (params) {
  return request({
    url: '/base/api/sys-menu/list',
    method: 'get',
    params
  })
}

// 获取表单列表
export function getFormList (params) {
  return request({
    url: '/base/api/form-field-attr/list',
    method: 'get',
    params
  })
}

