const jsonServer = require('json-server');//在node里面使用json-server包
const db = require('./db.js');//引入mockjs配置模块
const path = require('path');
const Mock = require('mockjs');
let mock='/nuwa-sys-user';//定义路由根别名
let time=1000;//延时返回数据

//创建服务器
const server = jsonServer.create();//创建jsonserver 服务对象


//配置jsonserver服务器 中间件
server.use(jsonServer.defaults({
  static:path.join(__dirname, '/public'),//静态资源托管
}));

server.use(jsonServer.bodyParser);//抓取body数据使用json-server中间件


//响应
server.use((req, res, next) => {//可选 统一修改请求方式
  console.log(req.query, req.body);//抓取提交过来的query和body
  // token统一携带的校验
  if (req.url.includes('/login')){
    next()
  } else {
    if (req.headers.authorization && req.headers.authorization.length>=5){
      next()
    } else {
      setTimeout(()=>{
        res.jsonp({
          code:401,
          msg:'token过期'
        })
      },time)
    }
  }
});

//登录注册校验
let mr = Mock.Random;//提取mock的随机对象

server.post(mock+'/login', (req, res) => {
  console.log(req.query, req.body);//抓取提交过来的query和body
  res.jsonp({
    code:200,
    data: {
      token:'1234567890123456',
      tokenHead:'tokenHead'
    }
  }) 
});

// 首页
server.get('/base/api/application-manager/list',(req,res)=>{
  res.jsonp({
    code:200,
    msg:'success',
    data:[ 
      { appCode: "no12",appName: "mock应用1",id: 2},
      { appCode: "no13",appName: "mock应用2",id: 3}
    ]
  })
});
// 新建应用
server.post('/nuwa-application-manager/edit',(req,res)=>{
  res.jsonp({
    code:200,
    data:{ id: 6}
  })
});

















//响应mock接口
const router = jsonServer.router(db);//创建路由对象 db为mock接口路由配置  db==object

//自定义返回结构
router.render = (req, res) => {//自定义返回结构
  let len = Object.keys(res.locals.data).length; //判断数据是不是空数组和空对象
  // console.log(len);

  setTimeout(()=>{//模拟服务器延时
    res.jsonp({
      err: len !== 0 ? 0 : 1,
      msg: len !== 0 ? '成功' : '失败',
      data: res.locals.data
    })
  },time)

  // res.jsonp(res.locals.data)

};

//定义mock接口别名
server.use(jsonServer.rewriter({//路由自定义别名
  [mock+"/*"]: "/$1",

  "/api/*": "/$1",
  "/course/:id/check": "/course/:id",
  "/list?category=:category": "/list/:category",
  "/course/find\\?id=:id": "/course/:id"
}));

server.use(router);//路由响应

//开启jsonserver服务
server.listen(8080, () => {
  console.log('mock server is running')
});