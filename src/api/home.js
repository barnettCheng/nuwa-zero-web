/**
 * @description:首页下服务函数
 * @author:zhangmanyi
 */

import request from '@/utils/request'

// 查询应用管理列表
export function getAllApp () {
  return request({
    url: '/base/api/application-manager/list',
    method: 'get'
  })
}

// 根据应用Id删除应用
export function deleteApp (data) {
  return request({
    url: '/base/api/application-manager/del',
    method: 'post',
    data
  })
}