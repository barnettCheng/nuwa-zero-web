export function IdToTag(code) {
  let type = {
    single_line_text: 'el-input', // 单行文本
    multiline_text: 'el-input', // 多行文本
    numeral: '', // 数字
    date_time: 'el-date-picker', // 日期时间
    radio_button_group: '', // 单选按钮组
    checx_box_group: '', // 复选框组
    drop_down_check_box: '', // 下拉复选框
    drop_down_box: 'el-select', // 下拉框
    split_line: '', // 分割线
    form_table: '', // 表格
    address: '', // 地址
    location: '', // 定位
    picture: '', // 图片
    attachment: 'el-upload', // 附件
    sub_form: 'subform', // 子表单
    relation_query: '', // 关联查询
    relaction_data: '', // 关联数据
    serial_number: '', // 流水号
    mobile_phone: '', // 手机
    character_recognition: '', // 文字识别
    signature: '', // 手写签名
    member_radio: '', // 成员单选
    member_selection: '', // 成员多选
    dept_radio: '', // 部门单选
    dept_selection: '' // 部门多选
  }[code]
  return type
}

export function IdToMineTag(code) {
  let type = {
    single_line_text: 'mine-el-input', // 单行文本
    multiline_text: 'el-input', // 多行文本
    numeral: '', // 数字
    date_time: 'el-date-picker', // 日期时间
    radio_button_group: '', // 单选按钮组
    checx_box_group: '', // 复选框组
    drop_down_check_box: '', // 下拉复选框
    drop_down_box: 'el-select', // 下拉框
    split_line: '', // 分割线
    form_table: '', // 表格
    address: '', // 地址
    location: '', // 定位
    picture: '', // 图片
    attachment: 'el-upload', // 附件
    sub_form: 'subform', // 子表单
    relation_query: '', // 关联查询
    relaction_data: '', // 关联数据
    serial_number: '', // 流水号
    mobile_phone: '', // 手机
    character_recognition: '', // 文字识别
    signature: '', // 手写签名
    member_radio: '', // 成员单选
    member_selection: '', // 成员多选
    dept_radio: '', // 部门单选
    dept_selection: '' // 部门多选
  }[code]
  return type
}

export function tagToIdType(code) {
  // 标签转换id
  let idTypeArr
  let tagType = ['base_code', 'strong_code', 'dept_code'] // 标签三种类型 简单、复杂、增强
  switch(code) {
    case 'el-input':
      idTypeArr = ['single_line_text', tagType[0]]
      break;
    case 'el-date-picker':
      idTypeArr = ['date_time', tagType[0]]
      break;
    case 'el-select':
      idTypeArr = ['drop_down_box', tagType[0]]
      break;
    case 'el-upload':
      idTypeArr = ['attachment', tagType[1]]
      break;
    case 'subform':
      idTypeArr = ['sub_form', tagType[1]]
      break;
  }
  return idTypeArr
}