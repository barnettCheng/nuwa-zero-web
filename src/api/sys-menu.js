/**
 * @description:菜单管理服务函数
 * @author:zhangmanyi
 */

import request from '@/utils/request'

// 查询应用管理列表
export function getAllMenus (params) {
  return request({
    url: '/base/api/sys-menu/all',
    method: 'get',
    params
  })
}

// 删除菜单
export function deleteMenu (data) {
  return request({
    url: '/base/nuwa-sys-menu/del',
    method: 'post',
    data
  })
}

// 编辑菜单 修改&新增
export function editMenu (data) {
  return request({
    url: '/base/api/sys-menu/edit',
    method: 'post',
    data
  })
}

