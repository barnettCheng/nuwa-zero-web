/**
 * @description:用户注册登录服务函数
 * @author:zhangmanyi
 */
import request from '@/utils/request'

// 登录
export function login(data) {
  return request({
    url: '/auth/login',
    method: 'post',
    data
  })
}

// 根据表统一认证号查询用户信息
export function queryUserInfo(data) {
  return request({
    url: '/base/sys-user/queryUserInfo',
    method: 'post',
    data
  })
}