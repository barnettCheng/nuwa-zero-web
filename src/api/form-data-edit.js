import request from '@/utils/request'

export function saveOrUpdateFormData (data) {
  return request({
    url: '/base/api/form-data/saveOrUpdate',
    method: 'post',
    data
  })
}

export function getFormData (params) {
  return request({
    url: '/base/api/form-data/list',
    method: 'get',
    params
  })
}

// 删除表单列表
export function deleteFormList (data) {
  return request({
    url: '/base/api/form-data/del',
    method: 'post',
    data
  })
}
