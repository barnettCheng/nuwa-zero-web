import Vue from 'vue'
import Vuex from 'vuex'

// 通过require.context的方式自动注册模块，无需手动引用注册modules下模块
const modulesFiles = require.context('./modules', true, /\.js$/)
const modules = modulesFiles.keys().reduce((modules, modulePath) => {
  const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
  const value = modulesFiles(modulePath)
  modules[moduleName] = value.default
  return modules
}, {})

Vue.use(Vuex)

const store = new Vuex.Store({
  modules
})

export default store
