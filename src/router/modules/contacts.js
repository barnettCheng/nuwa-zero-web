const contacts = {
  path: '/contacts',
  component: () => import('@/views/contacts'),
  children: [
    {
      path: '/',
      redirect: '/contacts/internal'
    },
    {
      path: 'internal',
      component: () => import('@/views/contacts/views/internal'),
    },
    {
      path: 'interconnected',
      component: () => import('@/views/contacts/views/interconnected'),
    },
    {
      path: 'manager',
      component: () => import('@/views/contacts/views/manager'),
    }
  ]
}

export default contacts