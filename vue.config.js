const { defineConfig } = require('@vue/cli-service')
const path = require('path')
const resolve = dir => path.join(__dirname, dir)

module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: false,
  devServer: {
    // client: {
    //   webSocketURL: 'ws://0.0.0.0:6103/ws',
    // },
    host: 'localhost',
    // 监听端口
    port: 9090, 
    // 配置跨域请求头，解决开发环境的跨域问题
    headers: {
      'Access-Control-Allow-Origin': '*'
    },
    open: true,
    proxy: {
      "^/":{
        target:"http://localhost:8080/",
        ws: false
      }
    }
  },
  configureWebpack: {
    output: {
      // 微应用的包名，这里与主应用中注册的微应用名称一致
      library: "vue-app",
      // 将你的 library 暴露为所有的模块定义下都可运行的方式
      libraryTarget: "umd",
    },
  },
  chainWebpack: (config) => {
    // 配置 svg-sprite-loader
    config.module.rules.delete('svg') // 删除默认配置中处理svg
    config.module
      .rule('svg-sprite-loader')
      .test(/\.svg$/)
      .include.add(resolve('src/assets/svg-icon')) // 处理svg目录
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
  }
})
